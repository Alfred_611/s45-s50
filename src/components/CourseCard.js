// import { useState } from 'react';
// Proptypes - used to validate props
import PropTypes from 'prop-types'
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	//console.log(props);
	const {_id, name, description, price} = courseProp;

	// State hook - used to keep track of information related to individual components
	// Syntax: const [getter, setter] = useState(initialGetterValue);
	/*const [count, setCount] = useState(0);
	const [seat, seats] = useState(30);

	function enroll(){
		setCount(count + 1);
		seats(seat - 1);
		if (seat === 0){
			setCount(count);
			seats(seat);
			alert('No more seats');
		}
	}*/

		return (
		<Card>
			<Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle>Description:</Card.Subtitle>
				    <Card.Text>PhP {price}</Card.Text>
				    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link> 
				  </Card.Body>
				</Card>
		)
}

// Checks the validity of the PropTypes
CourseCard.propTypes = {
	// "shape" method is used to check if a prop object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}