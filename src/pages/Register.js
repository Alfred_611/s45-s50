import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){
	// State hooks to store the values of the input fields
	const [firstname, setFirstName] = useState('');
	const [lastname, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobilenumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		e.preventDefault();

		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNumber('');
		setPassword1('');
		setPassword2('');

		// Set the global user state to have properties obtained from our local storage
		setUser({
			email: localStorage.getItem('email')
		});

		Swal.fire({
					title: "Registration successful!",
					icon: "success",
					text: "Welcome to Zuitt!"
				});
		<Redirect to="/login" />
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match 
		if((firstname !== '' && lastname !== '' && email !== '' && mobilenumber !== '' && password1 !=='' && password2 !=='') && (password1 === password2))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstname, lastname, email, mobilenumber, password1, password2])

	return (	
		(user.id !== null) ?
			<Redirect to="/login" />
			:
		<Form onSubmit={(e) => registerUser(e)}>

			{/*Bind the input states via 2-way binding*/}
				<Form.Group controlId="userFirstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control
			     	type="firstname" 
			     	placeholder="Enter first name" 
			     	value={firstname}
			     	onChange={e => setFirstName(e.target.value)}
			     	required />
			  	</Form.Group>



			  	<Form.Group controlId="userLastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control
			     	type="lastname" 
			     	placeholder="Enter last name" 
			     	value={lastname}
			     	onChange={e => setLastName(e.target.value)}
			     	required />
			  	</Form.Group>



			  	<Form.Group controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  	</Form.Group>



			  	<Form.Group controlId="userMobileNumber">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control
			     	type="mobilenumber" 
			     	placeholder="Enter mobile number" 
			     	value={mobilenumber}
			     	onChange={e => setMobileNumber(e.target.value)}
			     	required
			     	minlength ="11" />
			  	</Form.Group>



			  	<Form.Group controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control
			     	type="password" 
			     	placeholder="Password" 
			     	value={password1}
			     	onChange={e => setPassword1(e.target.value)}
			     	required />
			  	</Form.Group>



			  	<Form.Group controlId="password2">
			  	<Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			     	onChange={e => setPassword2(e.target.value)}
			    	required />
			  	</Form.Group>



				{/*Conditionally render the submit button based on isActive state*/}
				{ isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
			 		   Submit
			 		</Button>
			 		:
			 		<Button variant="danger" type="submit" id="submitBtn" disabled>
			  		   Submit
			  		</Button>
				 }
				  
		</Form>
	)
}